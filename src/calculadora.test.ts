import { calcular } from './calculadora';
import { OPERACION_SUMA } from './constants';

describe('calculadora.test', () => {
  it('Probar la suma', () => {
    expect(calcular(1, 2, OPERACION_SUMA)).toBe(3);
  });

  it('probar que te mande un error si le mandas a la función valores no númericos', () => {

  });
});
