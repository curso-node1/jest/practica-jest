import { OPERACION_SUMA } from './constants';

/**
 *
 * @param numero1 - Primero número a sumar
 * @param numero2 - Segundo número a sumar
 * @param operacion - sumar, restar, dividir, multiplicar
 */
function calcular(numero1: number, numero2: number, operacion: string): number {
  let resultado: number;
  if (operacion === OPERACION_SUMA) {
    resultado = numero1 + numero2;
  }
  return resultado;
}

export {
  calcular,
};
